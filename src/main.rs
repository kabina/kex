/// Kabina minibus/taxi dispatcher (extender only version)
/// Copyright (c) 2023 by Bogusz Jelinski bogusz.jelinski@gmail.com

mod repo;
mod model;
mod distance;
mod extender;
mod stats;
mod utils;
mod simul;
use model::{KernCfg, Order, OrderStatus, Stop, Cab, CabStatus, Bus, SimOrder};
use simul::{get_cfg, read_cfg_file, init_cabs, simul_cabs_orders};
use stats::{Stat, update_max_and_avg_time};
use repo::CNFG;
use distance::DIST;
use extender::{find_matching_routes, split_sql, write_sql_to_file};
use utils::get_elapsed;
use postgres::{Client, NoTls, Error};
use chrono::{Local, Duration};
use std::collections::HashMap;
use std::time::{Instant, SystemTime};
use std::{thread, vec, cmp, clone};
use std::env;
use rand::rngs::ThreadRng;
use hungarian::minimize;
use log::{info, warn, debug, LevelFilter};
use log4rs::{
    append::{
        console::{ConsoleAppender, Target},
        file::FileAppender,
    },
    config::{Appender, Config, Root},
    encode::pattern::PatternEncoder,
    filter::threshold::ThresholdFilter,
};

use crate::utils::get_elapsed_unwrapped;

const CFG_FILE_DEFAULT: &str = "kex.toml";

fn main() -> Result<(), Error> {
    // reading Config
    let mut cfg_file: String = CFG_FILE_DEFAULT.to_string();
    // command line arguments
    let args: Vec<String> = env::args().collect();
    if args.len() == 3 && args[1] == "-f" {
        cfg_file = args[2].to_string();
    }
    info!("Config file: {cfg_file}");
    let settings = config::Config::builder()
        .add_source(config::File::with_name(&cfg_file))
        .build()
        .unwrap();
    let cfg = settings
        .try_deserialize::<HashMap<String, String>>()
        .unwrap();
    let db_conn_str = &cfg["db_conn"];

    info!("Database: {db_conn_str}");
    unsafe {
        CNFG = KernCfg { 
            max_assign_time: cfg["max_assign_time"].parse().unwrap(),
            max_legs:        cfg["max_legs"].parse().unwrap(),
            max_angle:       cfg["max_angle"].parse::<f32>().unwrap(),
            thread_numb:     cfg["thread_numb"].parse().unwrap(),
            stop_wait:       cfg["stop_wait"].parse().unwrap(),
            cab_speed:       cfg["cab_speed"].parse().unwrap(),
        };
    }
    setup_logger(cfg["log_file"].clone());
    unsafe { info!("Starting up with config:"); 
        info!("max_assign_time: {}", CNFG.max_assign_time);
        info!("max_legs: {}", CNFG.max_legs);
        info!("max_angle: {}", CNFG.max_angle);
        info!("thread_numb: {}", CNFG.thread_numb);
        info!("stop_wait: {}", CNFG.stop_wait);
        info!("cab_speed: {}", CNFG.cab_speed);
    }
    // init DB
    let mut client = Client::connect(&db_conn_str, NoTls)?; // 192.168.10.176
    let stops = repo::read_stops(&mut client);
    distance::init_distance(&stops);

    // init simulator
    let sim_cfg_src = read_cfg_file("kim.toml".to_string());
    let sim_cfg: HashMap<String, u16> = get_cfg(&sim_cfg_src);
    let mut sim_cabs: Vec<Bus> = init_cabs(&mut client, sim_cfg["max_cab"], sim_cfg["max_stand"]);
    let mut sim_orders: Vec<SimOrder> = vec![];
    let mut rnd: ThreadRng = rand::thread_rng();
    let mut sim_cust_id: i64 = 0;
    let sim_started = SystemTime::now();
    let mut last_sim = SystemTime::now();
    //
    let mut itr = 0;
    let mut missed: usize = 0;
    loop {
        // get newly requested trips and free cabs, reject expired orders (no luck this time)
        let tmp_model = prepare_data(&mut client);
        match tmp_model {
            Some(mut x) => { 
                missed = dispatch(itr, &db_conn_str, &mut client, &mut x.0, &mut x.1, &stops);
            },
            None => {
                info!("Nothing to do");
            }
        }
        if missed == 0 {
            thread::sleep(std::time::Duration::from_millis(100));
        } // else run next iteration immediately

        // simulator
        if 2* get_elapsed_unwrapped(last_sim) > sim_cfg["check_interval"] as i64 {
            simul_cabs_orders(&mut client, &sim_cfg, &mut sim_cabs, &mut sim_orders,
                get_elapsed_unwrapped(sim_started) < sim_cfg["max_time"] as i64 * 60,
                2* (sim_cfg["req_per_min"] as f32 * get_elapsed_unwrapped(last_sim) as f32/60.0) as u16,
                &mut sim_cust_id, &mut rnd);
            last_sim = SystemTime::now();
        }
        itr += 1;
    }
}

fn setup_logger(file_path: String) {
    let level = log::LevelFilter::Info;
    // Build a stderr logger.
    let stderr = ConsoleAppender::builder().target(Target::Stderr).build();
    // Logging to log file.
    let logfile = FileAppender::builder()
        // Pattern: https://docs.rs/log4rs/*/log4rs/encode/pattern/index.html
        .encoder(Box::new(PatternEncoder::new("{d(%Y-%m-%d %H:%M:%S)} {l} - {m}\n")))
        .build(file_path)
        .unwrap();

    // Log Trace level output to file where trace is the default level
    // and the programmatically specified level to stderr.
    let config = Config::builder()
        .appender(Appender::builder().build("logfile", Box::new(logfile)))
        .appender(
            Appender::builder()
                .filter(Box::new(ThresholdFilter::new(level)))
                .build("stderr", Box::new(stderr)),
        )
        .build(
            Root::builder()
                .appender("logfile")
                .appender("stderr")
                .build(LevelFilter::Debug),
        )
        .unwrap();

    // Use this to change log levels at runtime.
    // This means you can change the default log level to trace
    // if you are trying to debug an issue and need more logs on then turn it off
    // once you are done.
    let _handle = log4rs::init_config(config);
}

fn run_extender(itr: i32, _thr_numb: i32, host: &String, client: &mut Client, orders: &Vec<Order>, stops: &Vec<Stop>, 
                max_leg_id: &mut i64, label: &str) -> (Vec<Order>, usize) {
    let demand: Vec<Order>;
    let len_before = orders.len();

    let start_extender = Instant::now();
    let ret = 
            find_matching_routes(itr, _thr_numb, &host, client, orders, &stops, max_leg_id, unsafe { &DIST });
    update_max_and_avg_time(Stat::AvgExtenderTime, Stat::MaxExtenderTime, start_extender);
    demand = ret.0;
    let len_after = demand.len();
    if len_before != len_after {
        info!("{}: route extender allocated {} requests, missed {} overlapping routes", label, len_before - len_after, ret.1);
    } else {
        info!("{}: extender has not helped", label);
    }
    return (demand, ret.1);
}

fn dispatch(itr: i32, host: &String, client: &mut Client, orders: &mut Vec<Order>, cabs: &mut Vec<Cab>, stops: &Vec<Stop>) -> usize {
    if orders.len() == 0 {
        info!("No demand, no dispatch");
        return 0;
    }
    let mut max_route_id : i64 = repo::read_max(client, "route"); // +1, first free ID
    let mut max_leg_id : i64 = repo::read_max(client, "leg");
    let thread_num: i32;
    unsafe {
        thread_num = CNFG.thread_numb;
    }

    let ret = run_extender(itr, thread_num, &host, client, orders, &stops, &mut max_leg_id, "FIRST");
    let mut demand = ret.0;

    let mut before_solver: i64 = max_route_id;
    let mut sql: String = String::from("");
    // shrinking vectors, getting rid of .id == -1 and (TODO) distant orders and cabs !!!!!!!!!!!!!!!
    if ret.1 == 0 { // expander has not found overlapping requests, orders that extend the same route, otherwise we need another iteration before we can run solver
        (*cabs, demand) = shrink(&cabs, demand);
        stats::update_max_and_avg_stats(Stat::AvgSolverDemandSize, Stat::MaxSolverDemandSize, demand.len() as i64);
        if cabs.len() == 0 {
            info!("No cabs after extender");
            return 0;
        }
        if demand.len() == 0 {
            info!("No demand after extender");
            return 0;
        }
        // SOLVER
        let start_solver = Instant::now();
        info!("Solver input - demand={}, supply={}", demand.len(), cabs.len());
        let sol = munkres(&cabs, &demand);
        before_solver = max_route_id;

        sql = repo::assign_cust_to_cab_munkres(sol, &cabs, &demand, &mut max_route_id, &mut max_leg_id);
        
        update_max_and_avg_time(Stat::AvgSolverTime, Stat::MaxSolverTime, start_solver);
    }
    sql += &repo::save_status();
    //write_sql_to_file(itr, &sql, "munkres");
    for s in split_sql(sql, 150) {
        match client.batch_execute(&s) { // here SYNC execution
            Ok(_) => {}
            Err(err) => {
                warn!("Solver SQL output failed to run {}, err: {}", s, err);
            }
        }
    }
    info!("Dispatch completed, solver assigned: {}", max_route_id - before_solver);
    return ret.1;
}

// remove orders and cabs allocated by the pool so that the vectors can be sent to solver
fn shrink (cabs: &Vec<Cab>, orders: Vec<Order>) -> (Vec<Cab>, Vec<Order>) {
    let mut new_cabs: Vec<Cab> = vec![];
    let mut new_orders: Vec<Order> = vec![];
    // v.iter().filter(|x| x % 2 == 0).collect() ??
    for c in cabs.iter() { 
        if c.id != -1 { new_cabs.push(*c); }
    }
    for o in orders.iter() { 
        if o.id != -1 { new_orders.push(*o); }
    }
    return (new_cabs, new_orders);
}

// 1) get unassigned orders and free cabs, 
// 2) expire old orders
// 3) some orders and cabs are too distant, although som cabs may end their last legs soon
// TODO: cabs on last leg should be considered
fn prepare_data(client: &mut Client) -> Option<(Vec<Order>, Vec<Cab>)> {
    let mut orders = repo::find_orders_by_status_and_time(
                client, OrderStatus::RECEIVED , Local::now() - Duration::minutes(5));
    if orders.len() == 0 {
        info!("No demand");
        return None;
    }
    info!("Orders, input: {}", orders.len());
    
    orders = expire_orders(client, &orders);
    if orders.len() == 0 {
        info!("No demand, expired");
        return None;
    }
    let mut cabs = repo::find_cab_by_status(client, CabStatus::FREE);
    if orders.len() == 0 || cabs.len() == 0 {
        warn!("No cabs available");
        return None;
    }
    info!("Initial count, demand={}, supply={}", orders.len(), cabs.len());
    orders = get_rid_of_distant_customers(&orders, &cabs);
    if orders.len() == 0 {
      info!("No suitable demand, too distant");
      return None; 
    }
    cabs = get_rid_of_distant_cabs(&orders, &cabs);
    if cabs.len() == 0 {
      info!("No cabs available, too distant");
      return None; 
    }
    return Some((orders, cabs));
}

// TODO: bulk update
fn expire_orders(client: &mut Client, demand: & Vec<Order>) -> Vec<Order> {
    let mut ret: Vec<Order> = Vec::new();
    let mut ids: String = "".to_string();
    for o in demand.iter() {
      //if (o.getCustomer() == null) {
      //  continue; // TODO: how many such orders? the error comes from AddOrderAsync in API, update of Customer fails
      //}
        let minutes_rcvd = get_elapsed(o.received)/60;
        let minutes_at : i64 = get_elapsed(o.at_time)/60;
        unsafe {
        if (minutes_at == -1 && minutes_rcvd > CNFG.max_assign_time)
                    || (minutes_at != -1 && minutes_at > CNFG.max_assign_time) {
            ids = ids + &o.id.to_string() + &",".to_string();
        } else {
            ret.push(*o);
        }}
    }
    if ids.len() > 0 {
        let sql = ids[0..ids.len() - 1].to_string(); // remove last comma
        match client.execute(
            "UPDATE taxi_order SET status=6 WHERE id IN ($1);\n", &[&sql]) { //OrderStatus.REFUSED
            Ok(_) => {}
            Err(err) => {
                warn!("Expire orders failed for {}, err: {}", sql, err);
            }
        }
        debug!("{} refused, max assignment time exceeded", &ids);
    }
    return ret;
}

// if we find just one cab nearby - continue with this order
fn get_rid_of_distant_customers(demand: &Vec<Order>, supply: &Vec<Cab>) -> Vec<Order> {
    let mut ret: Vec<Order> = Vec::new();
    for o in demand.iter() {
      for c in supply.iter() {
        unsafe {
            if distance::DIST[c.location as usize][o.from as usize] as i32 <= o.wait { 
                // great, we have at least one cab in range for this customer
                ret.push(*o);
                break;
            }
        }
      }
    }
    return ret;
}

fn get_rid_of_distant_cabs(demand: &Vec<Order>, supply: &Vec<Cab>) -> Vec<Cab> {
    let mut ret: Vec<Cab> = Vec::new();
    for c in supply.iter() {
        for o in demand.iter() {
            unsafe {
                if distance::DIST[c.location as usize][o.from as usize] as i32 <= o.wait {
                    // great, we have at least one customer in range for this cab
                    ret.push(*c);
                    break;
                }
            }
        }
    }
    return ret;
}

// returns indexes of orders assigned to cabs - vec[1]==5 would mean 2nd cab assigned 6th order
fn munkres(cabs: &Vec<Cab>, orders: &Vec<Order>) -> Vec<i16> {
    let mut ret: Vec<i16> = vec![];
    let mut matrix: Vec<i32> = vec![];
    
    for c in cabs.iter() {
        for o in orders.iter() {
            unsafe {
                matrix.push(distance::DIST[c.location as usize][o.from as usize] as i32);
            }
        }
    }
    let assignment = minimize(&matrix, cabs.len() as usize, orders.len() as usize);
    
    for s in assignment {
        if s.is_some() {
            ret.push(s.unwrap() as i16);
        } else {
            ret.push(-1);
        }
    }
    return ret;
}

#[cfg(test)]
mod tests {
  use super::*;
  use serial_test::serial;

  fn test_orders_invalid() -> Vec<Order> {
    return vec![
        Order{ id: 1, from: 1, to: 2, wait: 10, loss: 50, dist: 2, shared: true, in_pool: false,
            received: None, started: None, completed: None, at_time: None,eta: 0, route_id: -1},
        Order{ id: -1, from: 1, to: 2, wait: 10, loss: 50, dist: 2, shared: true, in_pool: false,
            received: None, started: None, completed: None, at_time: None,eta: 0, route_id: -1}
    ];
  }

  fn test_cabs_invalid() -> Vec<Cab> {
    return vec![
        Cab{ id: 1, location: 0},
        Cab{ id: -1, location: 1}
    ];
  }

  #[test]
  #[serial]
  fn test_shrink() {
    let orders: Vec<Order> = test_orders_invalid();
    let cabs: Vec<Cab> = test_cabs_invalid();
    assert_eq!(cabs.len(), 2);
    assert_eq!(orders.len(), 2);
    let ret = shrink(&cabs, orders);
    assert_eq!(ret.0.len(), 1);
    assert_eq!(ret.1.len(), 1);
  }

  #[test]
  fn test_munkres() {
    let orders: Vec<Order> = test_orders_invalid();
    let cabs: Vec<Cab> = test_cabs_invalid();
    let ret = munkres(&cabs, &orders);
    assert_eq!(ret.len(), 2);
  }

  #[test]
  #[serial]
  fn test_get_rid_of_distant_cabs() {
    let orders: Vec<Order> = test_orders_invalid();
    let cabs: Vec<Cab> = test_cabs_invalid();
    let ret = get_rid_of_distant_cabs(&orders, &cabs);
    assert_eq!(ret.len(), 2); // not distant
  }

  #[test]
  #[serial]
  fn test_get_rid_of_distant_orders() {
    let orders: Vec<Order> = test_orders_invalid();
    let cabs: Vec<Cab> = test_cabs_invalid();
    let ret = get_rid_of_distant_customers(&orders, &cabs);
    assert_eq!(ret.len(), 2); // not distant
  }

  #[test]
  #[serial]
  fn test_split_sql() {
    let sql: String  = String::from("UPDATE leg SET status=6, completed=now() WHERE id=31628;UPDATE route SET status=6 WHERE id=4;UPDATE cab SET status=0, location=1414 WHERE id=7296;UPDATE leg SET status=6, completed=now() WHERE id=31934;UPDATE route SET status=6 WHERE id=5;");
    let ret = split_sql(sql, 2);
    assert_eq!(ret.len(), 3); 
  }
}