use std::time::SystemTime;

pub fn get_elapsed(val: Option<SystemTime>) -> i64 {
    match val {
        Some(x) => { 
            match x.elapsed() {
                Ok(elapsed) => elapsed.as_secs() as i64,
                Err(_) => -1
            }
        }
        None => -1
    }
}

pub fn get_elapsed_unwrapped(val: SystemTime) -> i64 {
    return
        match val.elapsed() {
            Ok(elapsed) => elapsed.as_secs() as i64,
            Err(_) => -1
        }
}

pub fn get_elapsed_opt(val: Option<SystemTime>) -> i64 {
    match val {
        Some(x) => { 
            match x.elapsed() {
                Ok(elapsed) => elapsed.as_secs() as i64,
                Err(_) => -1
            }
        }
        None => -1
    }
}
