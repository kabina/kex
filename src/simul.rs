use std::collections::HashMap;
use std::time::SystemTime;
use log::{info, warn};
use rand::Rng;
use rand::rngs::ThreadRng;
use crate::model::{SimOrder, OrderStatus, Bus, Leg, BusStatus, CabStatus, RouteStatus};
use crate::repo::{read_orders, read_routes};
use crate::utils::{get_elapsed_opt, get_elapsed_unwrapped};
use crate::distance::DIST;
use crate::extender::split_sql;
use postgres::Client;

const CFG_ENTRIES: &'static [&'static str] = &[
    "db_conn",
    "cab_speed",
    "max_time", // duration of simulation
    "check_interval", // every how many senconds should the main loop execute
    "max_cab", // how many cabs are available 
    "max_stand", // number of stands/stops
    "stop_wait", // how long should a cab wait at the stop
    "req_per_min", // how many trips do customers request per min 
    "max_wait_for_assign", // the patience of a customer, how log can he put up with no answer from dispatcher
    "max_delay", // as with assignement, what is the acceptable delay beyond max_wait during pickup 
    "max_trip", // how long can a trip be, dispatcher's requirement
    "max_wait", // how long a customer want to wait for a cab
    "max_trip_actual", // 
    "max_trip_delay", // used to cancell trips that spanned much beyond trip duration + detour
    "max_detour" // max customers's loss in a pool, send together with max_wait in a request
];

pub fn simul_cabs_orders(client: &mut Client, cfg: &HashMap<String, u16>, cabs: &mut Vec<Bus>, 
                        orders: &mut Vec<SimOrder>, need_new_orders: bool, how_many: u16,
                        cust_id: &mut i64, rnd: &mut ThreadRng) {
    let iter_started = SystemTime::now();
    let mut sql: String = "".to_string();
    sql += &check_cabs(client, cabs, cfg["stop_wait"]);
    let check_cabs_elapsed = get_elapsed_unwrapped(iter_started);
    sql += &check_orders(client, orders, cabs, cfg);
    let check_orders_elapsed = get_elapsed_unwrapped(iter_started);
    if need_new_orders { // STOP generating after max_time
        sql += &gen_new_orders(cust_id, rnd, &cfg, how_many);
    }
    for s in split_sql(sql, 150) {
       client.batch_execute(&s);
    }
    // remove CANCELLED, COMPLETED
    orders.retain(|&o| o.status != OrderStatus::CANCELLED && o.status != OrderStatus::COMPLETED);
    info!("Iteration took {}s, cabs: {}s, orders:{}s", get_elapsed_unwrapped(iter_started),
            check_cabs_elapsed, check_orders_elapsed);
}

pub fn gen_new_orders(cust_id: &mut i64, rnd: &mut ThreadRng, cfg: &HashMap<String, u16>, n: u16) -> String {
    let mut sql: String = String::from("");
    for _ in 0..n {
        let (from, to) = rand_to_from(rnd, cfg["max_stand"], cfg["max_trip"]);
        if from == -1 {
            info!("Skipped order"); // just to know if it happens
            continue;
        }
        sql += &format!("INSERT INTO taxi_order (from_stand, to_stand, max_loss, max_wait, shared, in_pool, eta,\
                    status, received, distance, customer_id) VALUES ({},{},{},{},true,false,-1,0,now(),{},{});",
                    from, to, cfg["max_detour"], cfg["max_wait"],
                    unsafe { DIST[from as usize][to as usize]}, cust_id);
        info!("Request cust_id={}, from={} to={}", cust_id, from, to);
        *cust_id += 1;
    }
    return sql;
}

pub fn rand_to_from(rnd: &mut ThreadRng, max_stand: u16, max_trip: u16) -> (i32, i32) {
    // let us find a trip that <= max_time
    for _ in 0..30 { // tenth time is a charm
        let from: i16 = (*rnd).gen_range(0..max_stand) as i16;
        let mut diff: i16 = (*rnd).gen_range(0..max_trip * 2) as i16 - max_trip as i16;
        if diff == 0 {
            diff = 1
        }
        let to: i16;
        if from + diff > max_stand as i16 - 1 {
            to = from - diff;
        } else if from + diff < 0 {
            to = 0;
        } else {
            to = from + diff;
        }
        if unsafe {DIST[from as usize][to as usize]} <= max_trip as i16 {
            return (from as i32, to as i32);
        }
    }
    return (-1, -1);
}

pub fn check_orders(client: &mut Client, orders: &mut Vec<SimOrder>, cabs: &Vec<Bus>, cfg: &HashMap<String, u16>) -> String {
    let mut sql: String = String::from("");
    let db_orders: Vec<SimOrder> = read_orders(client);
    // update our internal represantion of orders with what we have got from DB
    for o in db_orders.clone()  {
        let mut idx: usize = usize::MAX;
        for (pos, order) in orders.clone().iter().enumerate() {
            if order.id == o.id {
                idx = pos as usize;
                break;
            }
        }
        if idx != usize::MAX { // update local info: status, cab,
            orders[idx].in_pool = o.in_pool;
            orders[idx].status = o.status;
            orders[idx].cab_id = o.cab_id;
            orders[idx].eta = o.eta;
            orders[idx].received = o.received;
            orders[idx].started = o.started;
        } else if o.status != OrderStatus::REFUSED {
            orders.push(o); // it might even be ASSIGNED at this time! or picked up!!! as we INSERTed this record in previous iteration
        }
    }
    // check if not too late or a cab has come?
    for (pos, o) in orders.clone().iter().enumerate()  {
        let cab_id: i64 =  match o.cab_id {
            Some(x) => { x }
            None => -1 // unassigned
        };
        if cab_id ==-1  && (o.status == OrderStatus::ASSIGNED || o.status == OrderStatus::PICKEDUP) {
            warn!("Error - no cab_id for ASSIGNED or PICKEDUP");
            continue;
        }
        match o.status {
            OrderStatus::RECEIVED => {
                if get_elapsed_opt(o.received) > cfg["max_wait_for_assign"] as i64 * 60 {
                    info!("Never assigned, cust_id={}", o.cust_id);
                    orders[pos].status = OrderStatus::CANCELLED;
                    sql += &format!("UPDATE taxi_order SET status=3 WHERE id={};", o.id);
                    continue;
                }
            },
            OrderStatus::ASSIGNED => {
                if o.assigned == None { // first time here; this timestamp is not available in DB now
                    orders[pos].assigned = Some(SystemTime::now());
                } 
                if cab_location(cab_id, &cabs) == o.from {
                    if get_elapsed_opt(o.assigned) > (cfg["max_wait"] as i64 + 1) * 60 { // +1: one min, come on!
                        info!("Cab came late, cust_id={}, order_id={}, cab_id={},", o.cust_id, o.id, cab_id);
                    }
                    info!("Picked up, cust_id={}, order_id={}, cab_id={},", o.cust_id, o.id, cab_id);
                    orders[pos].status = OrderStatus::PICKEDUP; // teoretically not needed (status will be updated from DB) but it happend that it wasn't
                    sql += &format!("UPDATE taxi_order SET status=7, started=now() WHERE id={};", o.id);
                    continue;
                } else if get_elapsed_opt(o.assigned) > cfg["max_wait"] as i64 * 60 
                        + cfg["max_delay"] as i64 * 60 {
                 // another topic - if we shouldn't count wait time from RECEIVED
                    orders[pos].status = OrderStatus::CANCELLED;
                    info!("Waited in vain, cust_id={}, order_id={}", o.cust_id, o.id);
                    sql += &format!("UPDATE taxi_order SET status=3 WHERE id={};", o.id);
                    continue;
                }
            },
            OrderStatus::PICKEDUP  => {
                let max_duration: i64 = (o.dist as f32 * (1.0 + (o.loss as f32 / 100.0)) 
                                         + cfg["max_trip_delay"] as f32) as i64;
                if cab_location(cab_id, &cabs) == o.to {
                    orders[pos].status = OrderStatus::COMPLETED; // to be removed from the list
                    if get_elapsed_opt(o.started) > max_duration * 60 {
                        info!("Completed late, cust_id={}, order_id={}, dist without pool={}, elapsed: {}", 
                                    o.cust_id, o.id, cab_id, get_elapsed_opt(o.started));
                    }
                    info!("Completed, cust_id={}, order_id={}, cab_id={},", o.cust_id, o.id, cab_id);
                    sql += &format!("UPDATE taxi_order SET status=8, completed=now() WHERE id={};", o.id);
                    continue;
                } else if get_elapsed_opt(o.started) > max_duration * 60 {
                    orders[pos].status = OrderStatus::CANCELLED;
                    info!("Destination not reached, cust_id={}, order_id={}, dist without pool={}, elapsed: {}", 
                                o.cust_id, o.id, cab_id, get_elapsed_opt(o.started));
                    sql += &format!("UPDATE taxi_order SET status=3 WHERE id={};", o.id);
                    continue;
                }
            },
            OrderStatus::REFUSED | OrderStatus::CANCELLED | OrderStatus::ABANDONED | OrderStatus::COMPLETED => {} // ignore it for now
        }
    }
    // remove CANCELLED and others from internal representation
    return sql;
}


pub fn cab_location(cab_id: i64, cabs: &Vec<Bus>) -> i32 {
    for cab in cabs {
        if cab.cab == cab_id {
            return cab.location;
        }
    }
    warn!("Cab not found, cab_id={},", cab_id);
    return -1;
}

pub fn check_cabs(client: &mut Client, cabs: &mut Vec<Bus>, stop_wait: u16) -> String {
    let mut sql: String = String::from("");
    let routes: Vec<Leg> = read_routes(client); // active routes
    if routes.len() == 0 {
        return sql;    
    }
    // update all cabs
    let mut prev_route = -1;
    
    for (pos, leg) in routes.iter().enumerate() {
        if leg.route_id != prev_route { // new route, we need the first leg only
            let is_last = pos == routes.len() - 1 || routes[pos + 1].route_id != leg.route_id;
            sql += &check_cab(leg.cab_id, cabs, *leg, stop_wait, is_last);
            prev_route = leg.route_id;
        }
    }
    return sql;
}

pub fn check_cab(cab_idx: i64, cabs: &mut Vec<Bus>, leg: Leg, stop_wait: u16, is_last: bool) -> String {
    let mut sql: String = String::from("");

    cabs[cab_idx as usize].leg = leg;
    
    let bus: Bus = cabs[cab_idx as usize].clone();
    let elapsed = get_elapsed_unwrapped(bus.started);
    if elapsed == -1 {
        warn!("Error - elapsed failed, cab_id={},", cab_idx);
        return sql;
    }
    match bus.status {
        BusStatus::UNASSIGNED => { // new route
            info!("New route to run, cab_id={}, route_id={},", cab_idx, leg.route_id);
            sql = update_route(leg.route_id, RouteStatus::STARTED);
            if bus.location != leg.from {
                warn!("Error - cab is not at the start of route, cab_id:{}, route_id: {}, location: {}", 
                        cab_idx, leg.route_id, bus.location);
                cabs[cab_idx as usize].location = leg.from;
            }
            if leg.passengers > 0 { // pick-up from the location, the first leg takes a passenger
                cabs[cab_idx as usize].status = BusStatus::WAITING;  // wait for the passenger, no DB update
                info!("Waiting at a stop: {}, cab_id={},", leg.from, cab_idx);
            } else {
                // otherwise go pickup the first one to the next stop
                sql += &update_leg_as_started(cab_idx, cabs, leg);
            }
            cabs[cab_idx as usize].started = SystemTime::now();
        },
        BusStatus::WAITING => {
            if elapsed > stop_wait as i64 {
                sql = update_leg_as_started(cab_idx, cabs, leg);
            } // else do nothing
        },    
        BusStatus::GOING => {
            if elapsed > leg.dist as i64 * 60 {
                sql = update_leg_as_completed(cab_idx, leg);
                
                if is_last { // last leg
                    cabs[cab_idx as usize].status = BusStatus::UNASSIGNED;
                    sql += &update_cab(cab_idx, leg.to, CabStatus::FREE);
                } else {
                    cabs[cab_idx as usize].status = BusStatus::WAITING;
                    cabs[cab_idx as usize].started = SystemTime::now();
                    sql += &update_cab(cab_idx, leg.to, CabStatus::ASSIGNED);
                }
                cabs[cab_idx as usize].location = leg.to;
            } 
        }
    }
    return sql;
}

pub fn update_leg_as_started(cab_idx: i64, cabs: &mut Vec<Bus>, leg: Leg) -> String {
    cabs[cab_idx as usize].status = BusStatus::GOING;
    cabs[cab_idx as usize].started = SystemTime::now();
    info!("Cab cab_id={}, is moving from {} to {}, leg_id={},", cab_idx, leg.from, leg.to, leg.id);
    return format!("UPDATE leg SET status=5, started=now() WHERE id={};", leg.id); // 5: STARTED
}

pub fn update_leg_as_completed(cab_idx: i64, leg: Leg) -> String {
    info!("Cab cab_id={}, reached the stop {}, leg_id={},", cab_idx, leg.to, leg.id);
    let mut sql = format!("UPDATE leg SET status=6, completed=now() WHERE id={};", leg.id); // 6: COMPLETED
    sql += &update_route (leg.route_id, RouteStatus::COMPLETED); // 6: COMPLETED
    return sql;
}

pub fn update_route(id: i64, status: RouteStatus) -> String {
    return format!("UPDATE route SET status={} WHERE id={};", status as i32, id);
}

pub fn update_cab(cab_idx: i64, location: i32, status: CabStatus) -> String {
    info!("Cab cab_id={}, has status {} at the stand {},", cab_idx, status, location);
    return format!("UPDATE cab SET status={}, location={} WHERE id={};", status as i32, location, cab_idx); 
}

pub fn init_cabs(client: &mut Client, max_cab: u16, max_stand: u16) -> Vec<Bus> {
    let sql: String;
    let mut cabs: Vec<Bus> = Vec::new();
    // making cabs free, two scenarios, try to spread cabs:
    if max_cab > max_stand {
        // parameters not sent as params as there was a strange error - a key should be i8
        sql = format!("UPDATE cab SET status=1, location=id % {} WHERE id < {};", max_stand, max_cab);
    } else {
        sql = format!("UPDATE cab SET status=1, location=id * ({}/{}) WHERE id < {};", max_stand, max_cab, max_cab);
    }
    match client.execute(&sql, &[]) {
        Ok(_) => {}
        Err(err) => {
            warn!("Update cabs failed for {}, err: {}", sql, err);
        }
    }

    for i in 0..max_cab {
        cabs.push(Bus { status: BusStatus::UNASSIGNED, 
                        leg: Leg { id: -1, route_id: -1, cab_id: -1, from: -1, to: -1, place: -1, dist: -1,
                                 reserve: -1, started: None, completed: None, status: RouteStatus::COMPLETED, passengers: -1 },
                        cab: i as i64,
                        started: SystemTime::now(),
                        location: if max_cab > max_stand { i %  max_stand } else { i * (max_stand/     max_cab) } as i32});
    }
    return cabs;
}

pub fn read_cfg_file(cfg_file: String) -> HashMap<String, String> {
    let settings = config::Config::builder()
        .add_source(config::File::with_name(&cfg_file))
        .build()
        .unwrap();
    return settings
        .try_deserialize::<HashMap<String, String>>()
        .unwrap();
}

pub fn get_cfg(cfg_src: &HashMap<String, String>) -> HashMap<String, u16> {
    let mut cfg: HashMap<String, u16> = HashMap::new();
    for v in CFG_ENTRIES {
        if v == &"db_conn" || v == &"log_file" { continue; }
        cfg.insert(v.to_string(), cfg_src[*v].parse().unwrap());
    }
    return cfg;
}
