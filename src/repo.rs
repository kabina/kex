use log::{debug, info};
use postgres::{Client, Row};
use chrono::{DateTime, Local};
use std::cmp;
use std::time::SystemTime;
use crate::model::{KernCfg,Order, OrderStatus, Stop, Cab, CabStatus, Leg, RouteStatus, SimOrder};
use crate::distance::DIST;
use crate::stats::{STATS, Stat, add_avg_element, update_val, count_average};
use crate::utils::get_elapsed;

// default config, overwritten by cfg file
pub static mut CNFG: KernCfg = KernCfg { 
    max_assign_time: 3, // min
    max_legs: 8,
    max_angle: 120.0,
    thread_numb: 4,
    stop_wait: 1,
    cab_speed: 60,
};

pub fn find_orders_by_status_and_time(client: &mut Client, status: OrderStatus, at_time: DateTime<Local>) -> Vec<Order> {
    let mut ret : Vec<Order> = Vec::new();
    let qry = "SELECT id, from_stand, to_stand, max_wait, max_loss, distance, shared, in_pool, \
               received, started, completed, at_time, eta, route_id FROM taxi_order o WHERE o.status = $1 \
               and (o.at_time is NULL or o.at_time < '".to_string() + &at_time.to_string() + &"') ORDER by route_id".to_string();
    for row in client.query(&qry, &[&(status as i32)]).unwrap() {
        ret.push(Order {
            id: row.get(0),
            from: row.get(1),
            to: row.get(2),
            wait: row.get(3),
            loss: row.get(4),
            dist: row.get(5),
            shared: row.get(6),
            in_pool: row.get(7),
            received: row.get::<usize,Option<SystemTime>>(8),
            started: row.get::<usize,Option<SystemTime>>(9),
            completed: row.get::<usize,Option<SystemTime>>(10),
            at_time: row.get::<usize,Option<SystemTime>>(11),
            eta: row.get(12),
            route_id: if matches!(status, OrderStatus::RECEIVED) { -1 } else { row.get(13) }
        });
    }
    return ret;
}

pub fn read_stops(client: &mut Client) -> Vec<Stop> {
    let mut ret: Vec<Stop> = Vec::new();
    for row in client.query("SELECT id, latitude, longitude, bearing FROM stop", &[]).unwrap() {
        ret.push(Stop {
            id: row.get(0),
            latitude: row.get(1),
            longitude: row.get(2),
            bearing: row.get(3)
        });
    }
    return ret;
}

pub fn read_max(client: &mut Client, table: &str) -> i64 {
    for row in client.query(&("SELECT MAX(id) FROM ".to_string() + &table.to_string()), &[]).unwrap() {
        let max: Option<i64> = row.get(0);
        return match max {
            Some(x) => { x + 1 }
            None => 1
        }
    }
    return 1; // no row
}

pub fn find_cab_by_status(client: &mut Client, status: CabStatus) -> Vec<Cab>{
    let mut ret: Vec<Cab> = Vec::new();
    for row in client.query("SELECT id, location FROM cab WHERE status=$1", 
                                &[&(status as i32)]).unwrap() {
        ret.push(Cab {
            id: row.get(0),
            location: row.get(1)
        });
    }
    return ret;
}

pub fn find_legs(client: &mut Client) -> Vec<Leg> {
    let mut ret: Vec<Leg> = Vec::new(); // OR status = 5 
    for row in client.query("SELECT id, from_stand, to_stand, place, distance, \
        started, completed, route_id, status, reserve, passengers FROM leg WHERE status = 1 OR status=5 \
        ORDER BY route_id ASC, place ASC", &[]).unwrap() {
        ret.push(Leg {
            id: row.get(0),
            from: row.get(1),
            to: row.get(2),
            place: row.get(3),
            dist: row.get(4),
            started: row.get::<usize,Option<SystemTime>>(5),
            completed: row.get::<usize,Option<SystemTime>>(6),
            route_id: row.get(7), 
            status: get_route_status(row.get(8)),
            reserve: row.get(9),
            passengers: row.get(10),
            cab_id: -1
        });
    }
    return ret;
}

pub fn get_route_status(idx: i32) -> RouteStatus {
    return unsafe { ::std::mem::transmute(idx as i8) };
}

pub fn assign_order_find_cab(order_id: i64, leg_id: i64, route_id: i64, eta: i32, in_pool: &str, called_by: &str) -> String {   
    debug!("Assigning order_id={} to route_id={}, leg_id={}, routine {}",
                                            order_id, route_id, leg_id, called_by);
    if leg_id == -1 {
        return format!("\
        UPDATE taxi_order AS o SET route_id={}, cab_id=r.cab_id, status=1, eta={}, in_pool={} \
        FROM route AS r WHERE r.id={} AND o.id={} AND o.status=0;\n", // it might be cancelled in the meantime, we have to be sure. 
        route_id, eta, in_pool, route_id, order_id);
    }
    return format!("\
        UPDATE taxi_order AS o SET route_id={}, leg_id={}, cab_id=r.cab_id, status=1, eta={}, in_pool={} \
        FROM route AS r WHERE r.id={} AND o.id={} AND o.status=0;\n", // it might be cancelled in the meantime, we have to be sure. 
        route_id, leg_id, eta, in_pool, route_id, order_id);
}

pub fn assign_order(order_id: i64, cab_id: i64, leg_id: i64, route_id: i64, eta: i16, in_pool: &str, called_by: &str) -> String {   
    debug!("Assigning order_id={} to cab_id={}, route_id={}, leg_id={}, routine {}",
                                            order_id, cab_id, route_id, leg_id, called_by);
    return format!("\
        UPDATE taxi_order SET route_id={}, leg_id={}, cab_id={}, status=1, eta={}, in_pool={} \
        WHERE id={} AND status=0;\n", // it might be cancelled in the meantime, we have to be sure. 
        route_id, leg_id, cab_id, eta, in_pool, order_id);
}

pub fn create_leg(order_id: i64, from: i32, to: i32, place: i32, status: RouteStatus, dist: i16, reserve: i32,
                  route_id: i64, max_leg_id: &mut i64, passengers: i8, called_by: &str) -> String {
    debug!("Adding leg to route: leg_id={}, route_id={}, order_id={}, from={}, to={}, place={}, distance={}, reserve={}, routine {}", 
                                *max_leg_id, route_id, order_id, from, to, place, dist,
                                cmp::max(reserve, 0), called_by);
    let ret = format!("\
        INSERT INTO leg (id, from_stand, to_stand, place, distance, status, reserve, route_id, passengers) VALUES \
        ({},{},{},{},{},{},{},{},{});\n", *max_leg_id, from, to, place, dist, status as u8, cmp::max(reserve, 0), route_id, passengers);
    *max_leg_id += 1;
    return ret;
}

pub fn update_leg_a_bit2(route_id: i64, leg_id: i64, to: i32, dist: i16, reserve: i32, passengers: i8) -> String {
    debug!("Updating existing route_id={}, leg_id={}, to={}, distance={}, reserve={}, passengers={}", 
                route_id, leg_id, to, dist, reserve, passengers);
    return format!("\
        UPDATE leg SET to_stand={}, distance={}, reserve={}, passengers={} WHERE id={};\n", 
        to, dist, reserve, passengers, leg_id);
}

pub fn update_place_in_legs_after(route_id: i64, place: i32) -> String {
    debug!("Updating places in route_id={} starting with place={}", route_id, place);
    return format!("UPDATE leg SET place=place+1 WHERE route_id={} AND place >= {};\n", route_id, place);
}

pub fn update_passengers_and_reserve_in_legs_between(route_id: i64, reserve: i32, place_from: i32, place_to: i32) -> String {
    if place_from > place_to {
        return "".to_string();
    }
    debug!("Updating passengers and reserve in route_id={}, reserve={} from place={} to place={}", 
                    route_id, reserve, place_from, place_to);
    return format!("\
        UPDATE leg SET passengers=passengers+1, reserve=LEAST(reserve, {}) WHERE route_id={} AND place BETWEEN {} AND {};\n", 
                    reserve, route_id, place_from, place_to);
}

pub fn update_reserve_after(route_id: i64, cost: i32, place_from: i32) -> String {
    if cost < 0 {
        return "".to_string();
    }
    debug!("Updating reserve in route_id={}, cost={} from place={}", route_id, cost, place_from);
    return format!("\
        UPDATE leg SET reserve=GREATEST(0, reserve-{}) WHERE route_id={} AND place >= {};\n", cost, route_id, place_from);
}

pub fn update_reserves_in_legs_before_and_including(route_id: i64, place: i32, wait_diff: i32) -> String {
    if place < 0 {
        return "".to_string();
    }
    debug!("Updating reserve in route_id={} starting with place={}, wait_diff={}", 
            route_id, place, wait_diff);
    return format!("\
        UPDATE leg SET reserve=LEAST(reserve, {}) WHERE route_id={} AND place <= {};\n", 
                wait_diff, route_id, place);
}

fn update_cab_add_route(cab: &Cab, order: &Order, place: &mut i32, eta: &mut i16, max_route_id: &mut i64, max_leg_id: &mut i64) -> String {
    // 0: CabStatus.ASSIGNED TODO: hardcoded status
    let mut sql: String = String::from("UPDATE cab SET status=0 WHERE id=");
    sql += &(cab.id.to_string() + &";\n".to_string());
    // alter table route alter column id add generated always as identity
    // ALTER TABLE route ADD PRIMARY KEY (id)
    // ALTER TABLE taxi_order ALTER COLUMN customer_id DROP NOT NULL;
    sql += &format!("INSERT INTO route (id, status, cab_id) VALUES ({},{},{});\n", // 0 will be updated soon
                    *max_route_id, 1, cab.id).to_string(); // 1=ASSIGNED

    if cab.location != order.from { // cab has to move to pickup the first customer
        unsafe {
            *eta = DIST[cab.location as usize][order.from as usize];
        }
        sql += &create_leg(order.id, cab.location, order.from, *place, RouteStatus::ASSIGNED, *eta, 0, // reserve,
                            *max_route_id, max_leg_id, 0, "assignCab");
        *place += 1;
        //TODO: statSrvc.addToIntVal("total_pickup_distance", Math.abs(cab.getLocation() - order.fromStand));
    }
    return sql;
}

fn assign_order_to_cab(order: Order, cab: Cab, place: i32, eta: i16, reserve: i32, route_id: i64, 
                    max_leg_id: &mut i64, called_by: &str) -> String {
    let mut sql: String = String::from("");
    unsafe {
        sql += &create_leg(order.id, order.from, order.to, place, RouteStatus::ASSIGNED, 
                       DIST[order.from as usize][order.to as usize], reserve, route_id, max_leg_id, 1, called_by);
    }
    sql += &assign_order(order.id, cab.id, *max_leg_id -1 , route_id, // -1 cause it is incremented in create_leg
                        eta, "false", "assignOrderToCab");
    add_avg_element(Stat::AvgOrderAssignTime, get_elapsed(order.received));
    return sql;
}

pub fn assign_cust_to_cab_munkres(sol: Vec<i16>, cabs: &Vec<Cab>, demand: &Vec<Order>, max_route_id: &mut i64, 
                            max_leg_id: &mut i64) -> String {
    let mut sql: String = String::from("");
    for (cab_idx, ord_idx) in sol.iter().enumerate() {
        if *ord_idx == -1 {
            continue; // cab not assigned
        }
        let order = demand[*ord_idx as usize];
        let cab: Cab = cabs[cab_idx];
        let mut place = 0;
        let mut eta = 0; // expected time of arrival, see comments in LCM above
        let mut reserve: i32 = order.wait - unsafe { DIST[cab.location as usize][order.from as usize] } as i32; // expected time of arrival
        if reserve < 0 { 
            // TODO/TASK we should communicate with the customer, if this is acceptable, more than WAIT TIME
            reserve = 0; 
        } 
        let loss = unsafe { DIST[order.from as usize][order.to as usize] as f32 * (order.loss as f32) / 100.0 } as i32 ;
        if reserve > loss { reserve = loss; } 
        sql += &update_cab_add_route(&cab, &order, &mut place, &mut eta, max_route_id, max_leg_id);
        sql += &assign_order_to_cab(order, cabs[cab_idx], place, eta, reserve, *max_route_id, max_leg_id, "assignCustToCabMunkres");
        *max_route_id += 1;
    }
    return sql;
}

pub fn save_status() -> String {
    let mut sql: String = String::from("");
    update_val(Stat::AvgOrderAssignTime, count_average(Stat::AvgOrderAssignTime));
    unsafe {
    for s in Stat::iterator() {
        sql += &format!("UPDATE stat SET int_val={} WHERE UPPER(name)=UPPER('{}');", STATS[*s as usize], s.to_string());
    }}
    return sql;
}

// simulator
pub fn read_routes(c: &mut Client) -> Vec<Leg> {
    let mut legs: Vec<Leg> = vec![];
    let leg_sql = "SELECT l.id, from_stand, to_stand, place, distance, started, completed, \
                        l.status, route_id, r.cab_id, passengers FROM leg l, route r 
                        WHERE route_id=r.id and (l.status=1 OR l.status=5) ORDER by route_id, place".to_string();
    let routes: Vec<Row>;

    match c.query(&leg_sql, &[]) {
        Ok(res) => { routes = res; }
        Err(err) => {
            info!("SQL error: {}", err); // most likely deadlock
            routes = c.query(&leg_sql, &[]).unwrap();
        }
    } 
    for row in routes {
        legs.push(Leg { 
            id:     row.get(0), 
            from:   row.get(1), 
            to:     row.get(2), 
            place:  row.get(3), 
            dist:   row.get(4), 
            started:row.get(5), 
            completed:row.get(6), 
            status: get_route_status(row.get(7)),
            route_id: row.get(8),
            cab_id: row.get(9),
            passengers: row.get(10),
            reserve: 0,
        });
    }
    return legs;
}

pub fn read_orders(c: &mut Client) -> Vec<SimOrder> {
    let mut ret: Vec<SimOrder> = vec![];
    // is there a bug in KERN, taxi_order with status=7 er missing cab_id ?? that's why a JOIN here, get cab_id from route:
    let sql = "SELECT id, cab_id, customer_id, from_stand, to_stand, max_loss, max_wait,\
                        shared, in_pool, eta, status, received, started, completed, distance, at_time \
                        FROM taxi_order WHERE status<3 OR status=6 OR status=7".to_string();
    let orders: Vec<Row>;

    match c.query(&sql, &[]) {
        Ok(res) => { orders = res; }
        Err(err) => {
            info!("SQL error: {}", err); // most likely deadlock
            // ok, now you can panic
            orders = c.query(&sql, &[]).unwrap();
        }
    } 

    for row in orders {
        ret.push(SimOrder { 
            id:     row.get(0), 
            cab_id: row.get(1), 
            cust_id:row.get(2), 
            from:   row.get(3), 
            to:     row.get(4), 
            loss:   row.get(5), 
            wait:   row.get(6), 
            shared: row.get(7), 
            in_pool:row.get(8), 
            eta:    row.get(9), 
            status: get_order_status(row.get(10)),
            received:row.get(11), 
            started:row.get(12), 
            completed:row.get(13), 
            dist:   row.get(14),
            at_time: row.get(15),
            assigned: None
        });
    }
    return ret;
}

pub fn get_order_status(idx: i32) -> OrderStatus {
    return unsafe { ::std::mem::transmute(idx as i8) };
}

#[cfg(test)]
mod tests {
  use super::*;
  pub const MAXORDERSNUMB: usize = 2000;
  fn init_test_data(order_count: u8) -> [Order; MAXORDERSNUMB] {
    let stop_count = 8;
    unsafe {
        for i in 0..stop_count { DIST[i][i+1]= 2 ; }
        for i in 0..order_count as usize { 
            DIST[i][stop_count -1 -i] = 2*(stop_count -1 -i*2) as i16;
        }
    }
    let o: Order = Order { id: 0, from: 0, to: stop_count as i32 - 1, wait: 10, loss: 90, dist: 7, shared: true, in_pool: true, 
                            received: None, started: None, completed: None, at_time: None, eta: 10, route_id: -1 };
    let mut orders: [Order; MAXORDERSNUMB] = [o; MAXORDERSNUMB];
    for i in 0..order_count as usize {
        let to: i32 = stop_count as i32 -1 -i as i32;
        unsafe{
            orders[i] = Order { id: i as i64, from: i as i32, to: to, wait: 10, loss: 90, dist: DIST[i as usize][to as usize] as i32, 
                            shared: true, in_pool: true, received: None, started: None, completed: None, at_time: None, eta: 10, route_id: -1 };
        }
    }
    return orders;
  }

 

/*
  #[test]
  fn test_check_route_reserve() {
    let order_count = 4;
    let br = get_test_branch(order_count);
    let orders = init_test_data(order_count);
    // this is a linear route, so we have to have 90% reserve of the shortest distance on that route
    // 90% of 1 is 0 (truncated int)
    // so therefore we had to use '2' as minimal distance
    assert_eq!(check_route_reserve(br, 0, &orders), 12); // 14*90%-14 = 12.6 (solo*loss - actual duration)
    assert_eq!(check_route_reserve(br, 3, &orders), 1); // 2*90%-1 = 1
  }
*/
}
